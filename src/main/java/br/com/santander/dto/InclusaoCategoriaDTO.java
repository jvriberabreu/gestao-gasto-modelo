package br.com.santander.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class InclusaoCategoriaDTO {

	private String descricao;
	
}
