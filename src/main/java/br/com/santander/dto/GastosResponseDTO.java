package br.com.santander.dto;

import java.util.Date;

import lombok.Data;

@Data
public class GastosResponseDTO {
	
	private Long id;
	private String descricao;
	private Double valor;
	private Date data;

}
