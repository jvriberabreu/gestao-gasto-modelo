package br.com.santander.dto;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class InclusaoGastosDTO {

	private String descricao;
	private Double valor;
	private Date data;
	
}
