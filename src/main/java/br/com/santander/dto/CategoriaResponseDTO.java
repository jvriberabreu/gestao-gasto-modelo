package br.com.santander.dto;

import lombok.Data;

@Data
public class CategoriaResponseDTO {

	private Long id;
	private String descricao;
	
}
