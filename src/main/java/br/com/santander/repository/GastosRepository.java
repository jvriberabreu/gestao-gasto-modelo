package br.com.santander.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.santander.entity.Gastos;

@Repository
public interface GastosRepository extends CrudRepository<Gastos, Long>{

}
