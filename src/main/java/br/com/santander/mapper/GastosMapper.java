package br.com.santander.mapper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import br.com.santander.dto.GastosResponseDTO;
import br.com.santander.dto.InclusaoGastosDTO;
import br.com.santander.entity.Gastos;

@Component
public class GastosMapper {

	Gastos toEntity(InclusaoGastosDTO gastosDto) {
		Gastos gastos = new Gastos();
		
		return gastos;
	}
	
	GastosResponseDTO toDTO() {
		GastosResponseDTO dto = new GastosResponseDTO();
		
		return dto;
	}
	
	List<GastosResponseDTO> toListDto(List<Gastos> listaGastos) {
		List<GastosResponseDTO> listDto = new ArrayList<>();
		GastosResponseDTO dto = new GastosResponseDTO();
		listaGastos.forEach(g -> {
			dto.setId(g.getCodigoUsuario());
			dto.setDescricao(g.getDescricao());
			dto.setValor(g.getValor());
			dto.setData(g.getData());
			listDto.add(dto);
		});
		
		return listDto;
	}
	
}
