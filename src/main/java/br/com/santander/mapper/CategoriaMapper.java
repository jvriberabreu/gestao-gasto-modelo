package br.com.santander.mapper;

import org.springframework.stereotype.Component;

import br.com.santander.dto.CategoriaResponseDTO;
import br.com.santander.dto.InclusaoCategoriaDTO;
import br.com.santander.entity.Categoria;

@Component
public class CategoriaMapper {
	
	Categoria toEntity(InclusaoCategoriaDTO categoriaDto) {
		Categoria categoria = new Categoria();

		return categoria;
	}
	
	CategoriaResponseDTO toDTO(Categoria cat) {
		CategoriaResponseDTO dto = new CategoriaResponseDTO();
		
		return dto;
	}

}
