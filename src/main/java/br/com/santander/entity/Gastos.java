package br.com.santander.entity;

import java.io.Serializable;
import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@RedisHash("Gastos")
@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class Gastos implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	private Long codigoUsuario;
	private String descricao;
	private Double valor;
	private Date data;

}
