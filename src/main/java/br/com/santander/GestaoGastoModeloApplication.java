package br.com.santander;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GestaoGastoModeloApplication {

	public static void main(String[] args) {
		SpringApplication.run(GestaoGastoModeloApplication.class, args);
	}

}
